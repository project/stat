<?php

function stat_admin_settings() {

}

/****************************************************************************
 * Demo example report.
 ****************************************************************************/
function stat_report_examples() {
  $sql = 'SELECT u.uid,
    u.name,
    sdt.device_type,
    sdb.device_brand,
    su.url,
    sur.url AS referrer,
    si.ip,
    sl.language,
    son.os_name,
    s.os_version,
    sbn.browser_name,
    s.browser_version,
    sr.resolution,
    s.created
    FROM {stat} s
    LEFT JOIN {users} u ON u.uid = s.uid
    LEFT JOIN {stat_device_type} sdt ON sdt.id = s.device_type
    LEFT JOIN {stat_device_brand} sdb ON sdb.id = s.device_brand
    LEFT JOIN {stat_url} su ON su.id = s.url
    LEFT JOIN {stat_url} sur ON sur.id = s.referrer
    LEFT JOIN {stat_ip} si ON si.id = s.ip
    LEFT JOIN {stat_language} sl ON sl.id = s.language
    LEFT JOIN {stat_os_name} son ON son.id = s.os_name
    LEFT JOIN {stat_browser_name} sbn ON sbn.id = s.browser_name
    LEFT JOIN {stat_resolution} sr ON sr.id = s.resolution
    ORDER BY s.sid DESC
    LIMIT 100';

  $result = db_query($sql);
  $stats = array();
  foreach ($result as $record) {
    $stats[] = (array)$record;
  }

  $latest_page_views = array();
  $latest_page_views['data']['header'] = array(
    array(
      'type' => 'string',
      'name' => 'uid',
      'value' => t('Uid'),
    ),
    array(
      'type' => 'string',
      'name' => 'name',
      'value' => t('Username'),
    ),
    array(
      'type' => 'string',
      'name' => 'ip',
      'value' => t('IP'),
    ),
    array(
      'type' => 'string',
      'name' => 'url',
      'value' => t('Url'),
    ),
    array(
      'type' => 'string',
      'name' => 'referrer',
      'value' => t('Referrer'),
    ),
    array(
      'type' => 'string',
      'name' => 'device_type',
      'value' => t('Device type'),
    ),
    array(
      'type' => 'string',
      'name' => 'device_brand',
      'value' => t('Device brand'),
    ),
    array(
      'type' => 'string',
      'name' => 'language',
      'value' => t('language'),
    ),
    array(
      'type' => 'string',
      'name' => 'os_name',
      'value' => t('OS name'),
    ),
    array(
      'type' => 'string',
      'name' => 'os_version',
      'value' => t('OS version'),
    ),
    array(
      'type' => 'string',
      'name' => 'browser_name',
      'value' => t('Browser name'),
    ),
    array(
      'type' => 'string',
      'name' => 'browser_version',
      'value' => t('Browser version'),
    ),
    array(
      'type' => 'string',
      'name' => 'resolution',
      'value' => t('Resolution'),
    ),
    array(
      'type' => 'string',
      'name' => 'created',
      'value' => t('Timestamp'),
    ),
  );

  foreach ($stats as $stat) {
    $content = array();
    foreach ($latest_page_views['data']['header'] as $header) {
      if (array_key_exists($header['name'], $stat)) {
        $content[] = $stat[$header['name']];
      }
    }
    $latest_page_views['data']['contents'][] = $content;
  }

  $render = array(
    '#theme' => 'stat_report_examples',
    '#stats' => array(
      'latest_page_views' => $latest_page_views,
    ),
  );
  return render($render);
}

/****************************************************************************
 * Page load activity report.
 ****************************************************************************/
function stat_report_pageload_activity() {
  $limit = 50;
  $headers = array(
    array('data' => t('Page'), 'field' => 'su.url'),
    array('data' => t('User'), 'field' => 'u.name',),
    t('System'),
    t('Device'),
    array('data' => t('Created'), 'field' => 's.created', 'sort' => 'desc'),
  );

  $query = db_select('stat', 's');
  $query->leftJoin('users', 'u', 'u.uid = s.uid');
  $query->leftJoin('stat_device_type', 'sdt', 'sdt.id = s.device_type');
  $query->leftJoin('stat_device_brand', 'sdb', 'sdb.id = s.device_brand');
  $query->leftJoin('stat_url', 'su', 'su.id = s.url');
  $query->leftJoin('stat_url', 'sur', 'sur.id = s.referrer');
  $query->leftJoin('stat_ip', 'si', 'si.id = s.ip');
  $query->leftJoin('stat_language', 'sl', 'sl.id = s.language');
  $query->leftJoin('stat_os_name', 'son', 'son.id = s.os_name');
  $query->leftJoin('stat_browser_name', 'sbn', 'sbn.id = s.browser_name');
  $query->leftJoin('stat_resolution', 'sr', 'sr.id = s.resolution');
  $query->addField('sur', 'url', 'referrer');
  $query
    ->fields('s', array('sid'))
    ->fields('u', array('uid', 'name'))
    ->fields('sdt', array('device_type'))
    ->fields('sdb', array('device_brand'))
    ->fields('su', array('url'))
    ->fields('sur', array('url'))
    ->fields('si', array('ip'))
    ->fields('son', array('os_name'))
    ->fields('s', array('os_version'))
    ->fields('sbn', array('browser_name'))
    ->fields('s', array('browser_version'))
    ->fields('sr', array('resolution'))
    ->fields('s', array('created'));

  $query = $query
    ->extend('PagerDefault')
    ->limit($limit);
  $query = $query
    ->extend('TableSort')
    ->orderByHeader($headers);

  $result = $query->execute();
  $rows = array();
  foreach ($result as $row) {
    $row = (array)$row;
    $geoip = stat_geoip($row['ip']);
    $geoip = '<pre>' . print_r($geoip['data'], TRUE) . '</pre>';

    $username = empty($row['uid']) ? t('User: ') . t('Anonymous') . '<div>IP: ' . $geoip . '</div>': t('User: ') . l($row['name'], 'user/' . $row['uid']) . '<div>IP: ' . $geoip . '</div>';
    $url = '<div>Page: ' . l($row['url'], $row['url']) . '</div>' . '<div>Referrer: ' . l($row['referrer'], $row['referrer']) . '</div>';
    $rows[] = array(
      array('data' => $url),
      array('data' => $username),
      array('data' => 'OS: ' . $row['os_name'] . ' ' . $row['os_version'] . '<br>' .
      'Browser: ' . $row['browser_name'] . ' ' . $row['browser_version']),
      array('data' => 'Type: ' . $row['device_type'] . '<br>'.
      'Brand: ' . $row['device_brand'] . '<br>'.
      'Resolution: ' . $row['resolution']),
      array('data' => format_date($row['created'], 'custom', 'Y-m-d') . '<br>' . format_date($row['created'], 'custom', 'H:i:s')),
    );
  }
  $output = theme('table', array(
      'header' => $headers,
      'rows' => $rows,
      'attributes' => array(
        'class' => array('stat-report-table'),
      )
    )
  );
  $output .= theme('pager', array('limit' => $limit));
  return $output;





  $render = array(
    '#theme' => 'stat_report_pageload_activity',
    '#stats' => array(),
  );
  return render($render);
}

function stat_report_pageload_activity_form()
{

}

/****************************************************************************
 * Visitors activity report.
 ****************************************************************************/

