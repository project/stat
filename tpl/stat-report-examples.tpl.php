<?php
$latest_page_views = $elements['#stats']['latest_page_views'];
?>


<script type="text/javascript" src="https://www.google.com/jsapi"></script>













<div id="table_div"></div>
<h2>Latest page views</h2>
<script type="text/javascript">
  var latestPageViews = <?php echo json_encode($latest_page_views); ?>;

  google.load("visualization", "1.1", {packages: ["table"]});
  google.setOnLoadCallback(drawTable);

  function drawTable() {
    var data = new google.visualization.DataTable();
    for (var i = 0; i < latestPageViews.data.header.length; i++) {
      data.addColumn(latestPageViews.data.header[i].type, latestPageViews.data.header[i].value);
    }

    data.addRows(latestPageViews.data.contents);

    var table = new google.visualization.Table(document.getElementById('table_div'));

    table.draw(data, {showRowNumber: false, page: 'enable', pageSize: 100});
  }
</script>



















<div id="piechart_3d" style="width: 500px; height: 500px;"></div>
<script type="text/javascript">
  google.load("visualization", "1.1", {packages:["corechart"]});
  google.setOnLoadCallback(drawBrowserChart);
  function drawBrowserChart() {
    var data = google.visualization.arrayToDataTable([
      ['Browsers', '%'],
      ['Windows', 11],
      ['Linux', 2],
      ['Max', 2]
    ]);

    var options = {
      title: 'Browsers',
      pieHole: 0.4,
      is3D: true
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
    chart.draw(data, options);
  }
</script>














<div id="columnchart_material" style="width: 500px; height: 500px;"></div>
<script type="text/javascript">
  google.load("visualization", "1.1", {packages:["bar"]});
  google.setOnLoadCallback(drawCompareVisitsChart);
  function drawCompareVisitsChart() {
    var data = google.visualization.arrayToDataTable([
      ['Monthly visits', 'Page views', 'Visitors', 'Returning Visits'],
      ['2014/01', 400, 150, 50],
      ['2014/02', 306, 120, 56],
      ['2014/03', 460, 90, 45],
      ['2014/04', 530, 80, 38]
    ]);

    var options = {
      chart: {
        title: 'Monthly reports',
        subtitle: ''
      }
    };

    var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

    chart.draw(data, options);
  }
</script>
