<?php
//print_r($stats);
//print_r($ip_data);
?>
<div class="stat-item">
  <div class="stat-user-info">
    <div>ISP: <?php echo $ip_data['isp']; ?></div>
    <div>City: <?php echo $ip_data['city']; ?></div>
    <div>Country: <?php echo $ip_data['country_code']; ?></div>
  </div>

  <div class="stat-user-info">
    <div>Date: <?php echo $created; ?></div>
  </div>
</div>

<style>
  .view-stat-pageload-activity-report .view-content {
    border: 1px solid #ccc;
    width: 100%;
  }
  .view-stat-pageload-activity-report .view-content .views-row {
    width: 100%;
    display: table;
  }
  .view-stat-pageload-activity-report .view-content .views-row-odd {
    background: #ededed;
  }
  .view-stat-pageload-activity-report .view-content .views-row-even {
    background: #fff;
  }
  .view-stat-pageload-activity-report .view-content .stat-item {
    padding: 15px;
  }
  .view-stat-pageload-activity-report .view-content .stat-item .stat-user-info {
    width: 30%;
    display: table-cell;
    vertical-align: top;
  }
</style>