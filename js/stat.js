/**
 * @file
 * Utility functions to handle the stat.
 */

(function ($) {

  Drupal.behaviors.stat = {
    attach: function (context, settings) {
      $.ajax({
        url: Drupal.settings.basePath + 'stat/record',
        type: 'POST',
        dataType: 'jsonp',
        data: {
          url: document.location.href,
          referrer: document.referrer,
          screenWidth: screen.width,
          screenHeight: screen.height
        },
        success: function (r) {

        }
      });
    }
  };

})(jQuery);
