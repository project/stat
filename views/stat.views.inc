<?php

/**
 * Implements hook_views_data()
 *
 */
function stat_views_data() {
  /**
   * Stat data.
   */
  $data['stat']['table']['group'] = t('Stat');
  $data['stat']['table']['base'] = array(
    'field' => 'sid',
    'title' => 'Stat',
    'help' => ''
  );
  $data['stat']['table']['join'] = array(
    'users' => array(
      'field' => 'uid',
      'left_field' => 'uid',
    ),
    'stat_browser_name' => array(
      'field' => 'id',
      'left_field' => 'browser_name',
    ),
    'stat_device_brand' => array(
      'field' => 'id',
      'left_field' => 'device_brand',
    ),
    'stat_device_type' => array(
      'field' => 'id',
      'left_field' => 'device_type',
    ),
    'stat_ip' => array(
      'field' => 'id',
      'left_field' => 'ip',
    ),
    'stat_language' => array(
      'field' => 'id',
      'left_field' => 'language',
    ),
    'stat_os_name' => array(
      'field' => 'id',
      'left_field' => 'os_name',
    ),
    'stat_resolution' => array(
      'field' => 'id',
      'left_field' => 'resolution',
    ),
    'stat_url' => array(
      'field' => 'id',
      'left_field' => 'url',
    ),
    'stat_referrer' => array(
      'table' => 'stat_url',
      'field' => 'id',
      'left_table' => 'stat',
      'left_field' => 'referrer',
    ),
  );
  $data['stat']['sid'] = array(
    'title' => t('Id'),
    'help' => t('The id of the stat record.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['uid'] = array(
    'title' => t('User'),
    'help' => t('The user who visited the site.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
    ),
  );
  $data['stat']['device_brand'] = array(
    'title' => t('Device type'),
    'help' => t('The id of the visitor\'s device brand.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['device_type'] = array(
    'title' => t('Device type'),
    'help' => t('The id of the visitor\'s device type.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['url'] = array(
    'title' => t('Url'),
    'help' => t('The id of the url that the visitor visits.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['referrer'] = array(
    'title' => t('Referrer url'),
    'help' => t('The id of the referrer url that the visitor comes from.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['search_engine_name'] = array(
    'title' => t('Search engine'),
    'help' => t('The search engine that the visitor used.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['search_engine_keyword'] = array(
    'title' => t('Search keyword'),
    'help' => t('The keyword that the visitor used to search.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['ip'] = array(
    'title' => t('IP'),
    'help' => t('The id of the visitor\'s IP address.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['language'] = array(
    'title' => t('Browser language'),
    'help' => t('The id of the visitor\'s browser language.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['os_name'] = array(
    'title' => t('OS name'),
    'help' => t('The id of the visitor\'s OS name.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['os_version'] = array(
    'title' => t('OS version'),
    'help' => t('The id of the visitor\'s OS version.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['browser_name'] = array(
    'title' => t('Browser name'),
    'help' => t('The id of the visitor\'s browser name.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['browser_version'] = array(
    'title' => t('Browser version'),
    'help' => t('The id of the visitor\'s browser version.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['resolution'] = array(
    'title' => t('Resolution'),
    'help' => t('The id of the visitor\'s device resolution.'),
    'field' => array(
      'handler' => 'views_handler_field_string',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['created'] = array(
    'title' => t('Created'),
    'help' => t('The timestamp that the visitor visited the site.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat']['custom_stat'] = array(
    'title' => t('Custom stat information'),
    'help' => t('A custom field to display a set of the stat information.'),
    'real field' => 'sid',
    'field' => array(
      'handler' => 'views_handler_field_stat',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  /**
   * Stat device type data.
   */
  $data['stat_device_type']['table']['group'] = t('Stat device type');
  $data['stat_device_type']['table']['join'] = array(
    'stat' => array(
      'left_table' => 'stat',
      'left_field' => 'device_type',
      'field' => 'id',
      'type' => 'LEFT',
    ),
  );
  $data['stat_device_type']['id'] = array(
    'title' => t('Id'),
    'help' => t('The id of the visitor\'s device type.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat_device_type']['device_type'] = array(
    'title' => t('Device type'),
    'help' => t('The value of the visitor\'s device type.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /**
   * Stat device brand data.
   */
  $data['stat_device_brand']['table']['group'] = t('Stat device brand');
  $data['stat_device_brand']['table']['join'] = array(
    'stat' => array(
      'left_table' => 'stat',
      'left_field' => 'device_brand',
      'field' => 'id',
      'type' => 'LEFT',
    ),
  );
  $data['stat_device_brand']['id'] = array(
    'title' => t('Id'),
    'help' => t('The id of the visitor\'s device brand.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat_device_brand']['device_brand'] = array(
    'title' => t('Device brand'),
    'help' => t('The value of the visitor\'s device brand.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /**
   * Stat ip data.
   */
  $data['stat_ip']['table']['group'] = t('Stat ip');
  $data['stat_ip']['table']['join'] = array(
    'stat' => array(
      'left_table' => 'stat',
      'left_field' => 'ip',
      'field' => 'id',
      'type' => 'LEFT',
    ),
  );
  $data['stat_ip']['id'] = array(
    'title' => t('Id'),
    'help' => t('The id of the visitor\'s IP address.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat_ip']['ip'] = array(
    'title' => t('IP'),
    'help' => t('The value of the visitor\'s IP address.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat_ip']['data'] = array(
    'title' => t('IP geo info'),
    'help' => t('The data of the visitor\'s IP geo information.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /**
   * Stat url data.
   */
  $data['stat_url']['table']['group'] = t('Stat url');
  $data['stat_url']['table']['join'] = array(
    'stat' => array(
      'left_table' => 'stat',
      'left_field' => 'url',
      'table' => 'stat_url',
      'field' => 'id',
      'type' => 'LEFT',
    ),
  );
  $data['stat_url']['id'] = array(
    'title' => t('Id'),
    'help' => t('The id of the url that the visitor visits.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat_url']['url'] = array(
    'title' => t('Url'),
    'help' => t('The value of the url that the visitor visits.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['stat_referrer']['table']['group'] = t('Stat referrer url');
  $data['stat_referrer']['table']['join'] = array(
    'stat' => array(
      'left_table' => 'stat',
      'left_field' => 'referrer',
      'table' => 'stat_url',
      'field' => 'id',
      'type' => 'LEFT',
    ),
  );
  $data['stat_referrer']['id'] = array(
    'title' => t('Id'),
    'help' => t('The id of the referrer url that the visitor comes from.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat_referrer']['url'] = array(
    'title' => t('Referrer url'),
    'help' => t('The value of the referrer url that the visitor comes from.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /**
   * Stat language data.
   */
  $data['stat_language']['table']['group'] = t('Stat language');
  $data['stat_language']['table']['join'] = array(
    'stat' => array(
      'left_table' => 'stat',
      'left_field' => 'language',
      'field' => 'id',
      'type' => 'LEFT',
    ),
  );
  $data['stat_language']['id'] = array(
    'title' => t('Id'),
    'help' => t('The id of the visitor\'s browser language.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat_language']['ip'] = array(
    'title' => t('Language'),
    'help' => t('The value of the visitor\'s browser language.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /**
   * Stat OS name data.
   */
  $data['stat_os_name']['table']['group'] = t('Stat OS name');
  $data['stat_os_name']['table']['join'] = array(
    'stat' => array(
      'left_table' => 'stat',
      'left_field' => 'os_name',
      'field' => 'id',
      'type' => 'LEFT',
    ),
  );
  $data['stat_os_name']['id'] = array(
    'title' => t('Id'),
    'help' => t('The id of the visitor\'s OS name.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat_os_name']['os_name'] = array(
    'title' => t('OS name'),
    'help' => t('The value of the visitor\'s OS name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /**
   * Stat browser name data.
   */
  $data['stat_browser_name']['table']['group'] = t('Stat browser name');
  $data['stat_browser_name']['table']['join'] = array(
    'stat' => array(
      'left_table' => 'stat',
      'left_field' => 'browser_name',
      'field' => 'id',
      'type' => 'LEFT',
    ),
  );
  $data['stat_browser_name']['id'] = array(
    'title' => t('Id'),
    'help' => t('The id of the visitor\'s browser name.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat_browser_name']['browser_name'] = array(
    'title' => t('Browser name'),
    'help' => t('The value of the visitor\'s browser name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  /**
   * Stat resolution data.
   */
  $data['stat_resolution']['table']['group'] = t('Stat resolution');
  $data['stat_resolution']['table']['join'] = array(
    'stat' => array(
      'left_table' => 'stat',
      'left_field' => 'resolution',
      'field' => 'id',
      'type' => 'LEFT',
    ),
  );
  $data['stat_resolution']['id'] = array(
    'title' => t('Id'),
    'help' => t('The id of the visitor\'s device resolution.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['stat_resolution']['resolution'] = array(
    'title' => t('Resolution'),
    'help' => t('The value of the visitor\'s device resolution.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}
