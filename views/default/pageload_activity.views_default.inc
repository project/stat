<?php

$view = new view();
$view->name = 'stat_pageload_activity_report';
$view->description = 'Report of page load activity.';
$view->tag = 'default';
$view->base_table = 'stat';
$view->human_name = 'Pageload activity report';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Pageload activity report';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'administer stat';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'created' => 'created',
  'created_1' => 'created',
  'custom_visitor' => 'custom_visitor',
  'browser_name' => 'browser_name',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'created' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '<div>',
    'empty_column' => 0,
  ),
  'created_1' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'custom_visitor' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'browser_name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Relationship: Stat: User */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'stat';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
/* Field: Stat: Custom stat information */
$handler->display->display_options['fields']['custom_stat']['id'] = 'custom_stat';
$handler->display->display_options['fields']['custom_stat']['table'] = 'stat';
$handler->display->display_options['fields']['custom_stat']['field'] = 'custom_stat';
$handler->display->display_options['fields']['custom_stat']['label'] = 'Date';
$handler->display->display_options['fields']['custom_stat']['stat'] = array(
  'fields' => array(
    'stat|created' => 'stat|created',
    'stat|sid' => 0,
    'stat_url|url' => 0,
    'stat_referrer|url' => 0,
    'stat|search_engine_name' => 0,
    'stat|search_engine_keyword' => 0,
    'stat_os_name|os_name' => 0,
    'stat|os_version' => 0,
    'stat_browser_name|browser_name' => 0,
    'stat|browser_version' => 0,
    'stat_device_brand|device_brand' => 0,
    'stat_device_type|device_type' => 0,
    'stat_ip|ip' => 0,
    'stat_ip|data' => 0,
    'stat_language|language' => 0,
    'stat_resolution|resolution' => 0,
    'users_stat|uid' => 0,
    'users_stat|name' => 0,
  ),
);
/* Field: Stat: Custom stat information */
$handler->display->display_options['fields']['custom_stat_2']['id'] = 'custom_stat_2';
$handler->display->display_options['fields']['custom_stat_2']['table'] = 'stat';
$handler->display->display_options['fields']['custom_stat_2']['field'] = 'custom_stat';
$handler->display->display_options['fields']['custom_stat_2']['label'] = 'Visitor';
$handler->display->display_options['fields']['custom_stat_2']['stat'] = array(
  'fields' => array(
    'users_stat|uid' => 'users_stat|uid',
    'users_stat|name' => 'users_stat|name',
    'stat|sid' => 0,
    'stat_url|url' => 0,
    'stat_referrer|url' => 0,
    'stat|search_engine_name' => 0,
    'stat|search_engine_keyword' => 0,
    'stat_os_name|os_name' => 0,
    'stat|os_version' => 0,
    'stat_browser_name|browser_name' => 0,
    'stat|browser_version' => 0,
    'stat_device_brand|device_brand' => 0,
    'stat_device_type|device_type' => 0,
    'stat_ip|ip' => 0,
    'stat_ip|data' => 0,
    'stat_language|language' => 0,
    'stat_resolution|resolution' => 0,
    'stat|created' => 0,
  ),
);
/* Field: Stat: Custom stat information */
$handler->display->display_options['fields']['custom_stat_1']['id'] = 'custom_stat_1';
$handler->display->display_options['fields']['custom_stat_1']['table'] = 'stat';
$handler->display->display_options['fields']['custom_stat_1']['field'] = 'custom_stat';
$handler->display->display_options['fields']['custom_stat_1']['label'] = 'System';
$handler->display->display_options['fields']['custom_stat_1']['stat'] = array(
  'fields' => array(
    'stat_os_name|os_name' => 'stat_os_name|os_name',
    'stat|os_version' => 'stat|os_version',
    'stat_browser_name|browser_name' => 'stat_browser_name|browser_name',
    'stat|browser_version' => 'stat|browser_version',
    'stat_device_brand|device_brand' => 'stat_device_brand|device_brand',
    'stat_device_type|device_type' => 'stat_device_type|device_type',
    'stat|sid' => 0,
    'stat_url|url' => 0,
    'stat_referrer|url' => 0,
    'stat|search_engine_name' => 0,
    'stat|search_engine_keyword' => 0,
    'stat_ip|ip' => 0,
    'stat_ip|data' => 0,
    'stat_language|language' => 0,
    'stat_resolution|resolution' => 0,
    'users_stat|uid' => 0,
    'users_stat|name' => 0,
    'stat|created' => 0,
  ),
);
/* Field: Stat: Custom stat information */
$handler->display->display_options['fields']['custom_stat_3']['id'] = 'custom_stat_3';
$handler->display->display_options['fields']['custom_stat_3']['table'] = 'stat';
$handler->display->display_options['fields']['custom_stat_3']['field'] = 'custom_stat';
$handler->display->display_options['fields']['custom_stat_3']['label'] = 'Web page';
$handler->display->display_options['fields']['custom_stat_3']['stat'] = array(
  'fields' => array(
    'stat_url|url' => 'stat_url|url',
    'stat_referrer|url' => 'stat_referrer|url',
    'stat|search_engine_name' => 'stat|search_engine_name',
    'stat|search_engine_keyword' => 'stat|search_engine_keyword',
    'stat|sid' => 0,
    'stat_os_name|os_name' => 0,
    'stat|os_version' => 0,
    'stat_browser_name|browser_name' => 0,
    'stat|browser_version' => 0,
    'stat_device_brand|device_brand' => 0,
    'stat_device_type|device_type' => 0,
    'stat_ip|ip' => 0,
    'stat_ip|data' => 0,
    'stat_language|language' => 0,
    'stat_resolution|resolution' => 0,
    'users_stat|uid' => 0,
    'users_stat|name' => 0,
    'stat|created' => 0,
  ),
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/reports/stat/pageload-activity';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Pageload activity report';
$handler->display->display_options['menu']['description'] = 'Report of page load activity.';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
