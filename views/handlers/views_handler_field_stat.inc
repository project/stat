<?php

class views_handler_field_stat extends views_handler_field {
  var $stat_fields;

  /**
   * Add some required fields needed on render().
   */
  function construct() {
    parent::construct();
    $this->set_stat_fields();
  }

  function set_stat_fields($fields = array()) {
    if (!empty($fields)) {
      $this->stat_fields = $fields;
    }
    else {
      $this->stat_fields = array(
        array('title' => t('Stat id'), 'table' => 'stat', 'field' => 'sid'),
        array('title' => t('Url'), 'table' => 'stat_url', 'field' => 'url'),
        array('title' => t('Referrer'), 'table' => 'stat_referrer', 'field' => 'url'),
        array('title' => t('Search engine name'), 'table' => 'stat', 'field' => 'search_engine_name'),
        array('title' => t('Search engine keyword'), 'table' => 'stat', 'field' => 'search_engine_keyword'),
        array('title' => t('OS name'), 'table' => 'stat_os_name', 'field' => 'os_name'),
        array('title' => t('OS version'), 'table' => 'stat', 'field' => 'os_version'),
        array('title' => t('Browser name'), 'table' => 'stat_browser_name', 'field' => 'browser_name'),
        array('title' => t('Browser version'), 'table' => 'stat', 'field' => 'browser_version'),
        array('title' => t('Device brand'), 'table' => 'stat_device_brand', 'field' => 'device_brand'),
        array('title' => t('Device type'), 'table' => 'stat_device_type', 'field' => 'device_type'),
        array('title' => t('IP'), 'table' => 'stat_ip', 'field' => 'ip'),
        array('title' => t('IP GEO'), 'table' => 'stat_ip', 'field' => 'data'),
        array('title' => t('Language'), 'table' => 'stat_language', 'field' => 'language'),
        array('title' => t('Resolution'), 'table' => 'stat_resolution', 'field' => 'resolution'),
        array('title' => t('User id'), 'table' => 'users_stat', 'field' => 'uid'),
        array('title' => t('User name'), 'table' => 'users_stat', 'field' => 'name'),
        array('title' => t('Created'), 'table' => 'stat', 'field' => 'created'),
      );
    }
  }

  /**
   * Default options form.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['stat'] = array('default' => '');
    return $options;
  }

  /**
   * Creates the form item for the options added.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $options = array();
    foreach ($this->stat_fields as $item) {
      $key = $item['table'] . '|' . $item['field'];
      $options[$key] = $item['title'];
    }
    $form['stat'] = array(
      '#type' => 'fieldset',
      '#title' => t('Stat fields'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['stat']['fields'] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $this->options['stat']['fields'],
      '#weight' => 0,
    );
  }

  function query() {
    foreach ($this->stat_fields as $item) {
      $this->query->add_field($item['table'], $item['field']);
    }
  }

  /**
   * Renders the field handler.
   */
  function render($values) {
//    dpm($this->options);
    $values = (array) $values;
    //dpm($values);
//    $data = array(
//      array(
//        'label' => t('Date'),
//        'value' => format_date($values['stat_created'], 'custom', 'Y-m-d'),
//      ),
//      array(
//        'label' => t('Time'),
//        'value' => format_date($values['stat_created'], 'custom', 'H:i:s'),
//      ),
//    );

    $data = array();
    if (isset($this->options['stat']['fields'])) {
      foreach ($this->options['stat']['fields'] as $k => $v) {
        if (!empty($v)) {
          foreach ($this->stat_fields as $field) {
            if ($k == $field['table'] . '|' . $field['field']) {
              $key = $field['table'] . '_' . $field['field'];
              $data[] = array(
                'label' => $field['title'],
                'value' => $values[$key],
              );
            }
          }
        }
      }
    }

    $output = '<table class="stat-inner-table">';
    foreach ($data as $item) {
      $output .= '<tr>';
      $output .= '<td class="label">' . $item['label'] . ':</td>';
      $output .= '<td class="value">' . $item['value'] . '</td>';
      $output .= '</tr>';
    }
    $output .= '</table>';

    return $output;
  }

}